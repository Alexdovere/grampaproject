//
//  HomeView.swift
//  GrampaProject
//
//  Created by Alessandro dovere on 12/05/2020.
//  Copyright © 2020 Alessandro dovere. All rights reserved.
//

import SwiftUI

struct HomeView: View {
  
    
    
    var body: some View {
        VStack {
            
            Text("Hello,\nWhat do you want to eat?")
                .font(.title)
                .fontWeight(.medium)
                .multilineTextAlignment(.leading)
                .padding(.horizontal)
            
            
            HStack(spacing: 15) {
                ForEach(categories){ category in
                    
                    CategoriesView(categories: category)
                    
                }
            }
            ScrollView {
                
                ScrollView(.horizontal,showsIndicators: false) {
                    HStack(spacing: 20) {
                        ForEach(cards) {card in
                            GeometryReader { geometry in
                                Card(card: card)
                                    .rotation3DEffect(
                                        Angle(degrees: Double(geometry.frame(in: .global).minX-30 )/18),
                                        axis: (x: 0, y: 10, z: 0)
                                )
                            }.frame(width: 190 , height: 190)
                        }
                    }.padding(30)
                }
            }
            
            
            
            Spacer()
        }.frame(maxWidth: .infinity)
        //          .padding(.top,44)
        
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

struct CategoriesView: View {
    @State var position = CGSize.zero
    
    var categories: Categories
    
    var body: some View {
        VStack{
         
            Image(categories.image)
                .resizable()
                .frame(width: 65,height: 65)
                .clipShape(Circle())
                .shadow(color: Color.black.opacity(0.4), radius: 5, x: 0, y: 2)
            
            Text(categories.name)
                .font(.subheadline)
                .fontWeight(.thin)
        }
            //        .offset(x: position.width, y: position.height)
            .gesture(
                DragGesture().onChanged{ value in
                    self.position = value.translation}
        )
    }
}




struct Card: View {
    var card: Cards
    var body: some View {
        ZStack {
            
            VStack {
                VStack(alignment: .leading) {
                    card.image
                        .resizable()
                        .aspectRatio(contentMode: .fill)
                        .shadow(color: Color.black.opacity(0.4), radius: 5, x: 0, y: 2)
                        .cornerRadius(30)
                    
                }.frame(width: 170,height: 120)
                    .padding(.top,5)
                    
                    .clipShape(RoundedRectangle(cornerRadius: 30, style: .continuous))
                
                
                
                HStack {
                    VStack(alignment: .leading) {
                        Text(card.name)
                            .font(.callout)
                        
                        HStack{
                            Text(card.position)
                            Spacer()
                            Text(card.cost)
                        }.frame(width: 160)
                    }
                    Spacer()
                }.frame(width: 160)
                
            }
        }
        .frame(width: 190, height: 190)
        .background(Color(#colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)))
        .cornerRadius(30)
        .shadow(color: Color(#colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)).opacity(0.3), radius: 20, x: 0, y: 2)
    }
}

struct Categories:Identifiable {
    var id = UUID()
    var name: String
    var image: String
}

struct Cards : Identifiable {
    var id = UUID()
    var cardCategory: String
    var image: Image
    var name: String
    var position: String
    var cost: String
}

let categories = [
    Categories(name: "All", image: "all"),
    Categories(name: "Restaurant", image: "pasta"),
    Categories(name: "Pizza", image: "pizza"),
    Categories(name: "Bakery", image: "cornetto")
    
]

let cards = [
    Cards(cardCategory: "Pizza", image: Image(uiImage: #imageLiteral(resourceName: "pizza")), name: "Pizzeria Gigio Franco", position: "Via Roma 69", cost: "15 €"),
    Cards(cardCategory: "Pizza", image: Image(uiImage: #imageLiteral(resourceName: "pizza")), name: "Pizzeria Gigio Franco", position: "Via Roma 69", cost: "15 €"),
    Cards(cardCategory: "Pizza", image: Image(uiImage: #imageLiteral(resourceName: "pizza")), name: "Pizzeria Gigio Franco", position: "Via Roma 69", cost: "15 €"),
    Cards(cardCategory: "Pizza", image: Image(uiImage: #imageLiteral(resourceName: "pizza")), name: "Pizzeria Gigio Franco", position: "Via Roma 69", cost: "15 €")

]
