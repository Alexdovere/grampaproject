//
//  LoginPageViewController.swift
//  GrampaProject
//
//  Created by Paolo Di Stasio on 13/05/2020.
//  Copyright © 2020 Alessandro dovere. All rights reserved.
//

import Foundation
import UIKit
import FirebaseUI

class LoginPageViewController: UIViewController{
    
     override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func LoginTapped(_ sender: UIButton) {
        
        //Get the default auth UI object
        let authUI = FUIAuth.defaultAuthUI()
        
        guard authUI != nil else {
            //log the error
            return
        }
        
        // set ourselves as the delegate
        authUI?.delegate = self
        
        //get a reference to the authUI view Controller
        let authViewController = authUI!.authViewController()
        
        //show it
        present(authViewController, animated: true)
    }

}

extension LoginPageViewController: FUIAuthDelegate {
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        //check if there was an error
        
        if error != nil {
            //log error
            return
        }
        
        //authDataResult?.user.uid
        
        performSegue(withIdentifier: "goHome", sender: self)
        
    }
}
